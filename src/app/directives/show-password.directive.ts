import { Directive, ElementRef, HostListener } from '@angular/core';
import { Input } from '@angular/core';

@Directive({
  selector: '[appShowPassword]'
})
export class ShowPasswordDirective {
  @Input() passwordInput: HTMLInputElement;

  constructor() { }

 @HostListener('mousedown')
  public onMouseDown() {
    this.passwordInput.type = 'text'
  }

  @HostListener('mouseup')
  public onMouseUp() {
    this.passwordInput.type = 'text'
  }

  @HostListener('mouseleave')
  public onMouseLeave() {
    this.passwordInput.type = 'password'
  }
}
