import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { DashbordModule } from './features/dashbord/dashbord.module';

import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';

import { AuthenticationService } from './services/authentication.service';

import { ShowPasswordDirective } from './directives/show-password.directive';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    ShowPasswordDirective
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    DashbordModule,
    ReactiveFormsModule,
  ],
  providers: [AuthenticationService],
  bootstrap: [AppComponent]
})
export class AppModule { }
