import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { AuthenticationService } from '../services/authentication.service';

@Injectable({
  providedIn: 'root',
})
export class LogOutGuard implements CanActivate {
  constructor(private readonly authService: AuthenticationService) {}
  canActivate() {
    if (this.authService.isUsserLoggedIn()) {
      const confirmLogOut = window.confirm('Are you sure you want to log out?');
      if (confirmLogOut) {
        sessionStorage.setItem('isUserLoggedIn', 'false');
        return true;
      } else {
        return false;
      }
    } else {
      return true;
    }
  }
}
