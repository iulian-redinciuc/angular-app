import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';

import { AuthenticationService } from '../services/authentication.service';

@Injectable()
export class AuthGuard implements CanActivate {
  constructor(private readonly authService: AuthenticationService, public readonly router: Router) {}

  canActivate(): boolean {
      if (!this.authService.isUsserLoggedIn()) {
        this.router.navigate(['login']);
        return false;
      }
      return true;
  }
}
