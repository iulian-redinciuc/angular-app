import {
  ChangeDetectionStrategy,
  Component,
  HostListener,
  OnInit,
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { catchError, Observable, tap } from 'rxjs';

import { DataService } from 'src/app/services/data.service';
import { ErorrHandlingService } from 'src/app/services/erorr-handling.service';

import { QuotesPagination } from 'src/app/interfaces/quotesPagination.interface';
import { Quote } from 'src/app/interfaces/quote.interface';
import { QuotesDataService } from '../services/quotes-data.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DashboardComponent implements OnInit {
  public page: number;
  public totalCount: number;
  public quotesTags: Observable<any>;
  public quotesPage: Observable<any>;
  public pagination: number;
  public innerWidth: number;
  private filterQuotesByTagName: string;

  @HostListener('window:resize', ['$event'])
  onResize() {
    this.innerWidth = window.innerWidth;
  }
  constructor(
    private readonly dataService: DataService,
    private readonly route: ActivatedRoute,
    private readonly router: Router,
    private readonly errorHandlingService: ErorrHandlingService,
    private readonly quotesDataService: QuotesDataService
  ) {}

  ngOnInit(): void {
    this.innerWidth = window.innerWidth;
    this.filterQuotesByTagName = this.route.snapshot.queryParams['tag'];
    this.pagination = this.route.snapshot.queryParams['page'];

    this.quotesPage = this.quotesDataService
      .checkForFilters(this.filterQuotesByTagName, this.pagination)
      .pipe(
        tap((quotes: QuotesPagination) => {
          this.mapQuotesPaginationInfo(quotes);
        }),
        catchError((error) => {
          return this.errorHandlingService.handleError(error);
        })
      );

    this.quotesTags = this.dataService.checkForQuotesTagsCache().pipe(
      catchError((error) => {
        return this.errorHandlingService.handleError(error);
      })
    );
  }

  handlePageChange(pageNumber: number): void {
    this.filterQuotesByTagName = this.route.snapshot.queryParams['tag'];

    this.router
      .navigate([], {
        relativeTo: this.route,
        queryParams: { page: pageNumber, tag: this.filterQuotesByTagName },
      })
      .then(() => {
        this.quotesPage = this.quotesDataService
          .handlePageChange(this.filterQuotesByTagName, pageNumber)
          .pipe(
            tap((quotes: QuotesPagination) => {
              this.mapQuotesPaginationInfo(quotes);
            }),
            catchError((error) => {
              return this.errorHandlingService.handleError(error);
            })
          );
      });
  }

  goToDetailsPage(quoteData: Quote): void {
    this.router.navigateByUrl(`dashboard/details/${quoteData._id}`, {
      state: { quote: quoteData },
    });
  }

  public getquotesByTagName(event: any): void {
    const value = event.target.value;
    if (value === 'all') {
      this.router.navigate([], {
        relativeTo: this.route,
      });
      this.pagination = 1;
      this.quotesPage = this.dataService.getQuotesPage(this.pagination).pipe(
        tap((quotes: QuotesPagination) => {
          this.mapQuotesPaginationInfo(quotes);
        }),
        catchError((error) => {
          return this.errorHandlingService.handleError(error);
        })
      );
    } else {
      this.router.navigate([], {
        relativeTo: this.route,
        queryParams: { tag: value, page: this.pagination },
      });
      this.quotesPage = this.dataService.getQuotesByTagName(value).pipe(
        tap((quotes: QuotesPagination) => {
          this.mapQuotesPaginationInfo(quotes);
        }),
        catchError((error) => {
          return this.errorHandlingService.handleError(error);
        })
      );
    }
  }

  private mapQuotesPaginationInfo(quotesDataResponse: QuotesPagination): void {
    this.totalCount = quotesDataResponse.totalCount;
    this.page = quotesDataResponse.page;
  }
}
