import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardRoutingModule } from './dashboard-roouting.module';
import { HttpClientModule } from '@angular/common/http';

import { NgxPaginationModule } from 'ngx-pagination';

import { QuoteComponent } from 'src/app/features/dashbord/components/quote/quote.component';
import { DashboardComponent } from './dashboard.component';
import { DetailsComponent } from './components/details/details.component';

import { DataService } from 'src/app/services/data.service';
import { ErorrHandlingService } from 'src/app/services/erorr-handling.service';
import { QuotesDataService } from '../services/quotes-data.service';


@NgModule({
  declarations: [DashboardComponent, QuoteComponent, DetailsComponent],
  imports: [
    CommonModule,
    DashboardRoutingModule,
    HttpClientModule,
    NgxPaginationModule
  ],
  providers: [DataService, ErorrHandlingService, QuotesDataService]
})
export class DashbordModule { }
