import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { DataService } from '../../../../services/data.service'

import { Quote } from 'src/app/interfaces/quote.interface';
import { ErorrHandlingService } from 'src/app/services/erorr-handling.service';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss']
})
export class DetailsComponent implements OnInit {
  quoteInHistoryState = window.history.state.quote;
  quote: Quote;

  constructor(private readonly dataService: DataService, private readonly route: ActivatedRoute, private readonly errorHandlingService: ErorrHandlingService) { }

  ngOnInit(): void {
    if(this.quoteInHistoryState) {
      this.quote = window.history.state.quote;;
    } else {
      this.dataService.getQuoteById(this.route.snapshot.params['id']).subscribe({
        next: (quote: Quote) => this.quote = quote,
        error: (error) => this.errorHandlingService.handleError(error)
      })
    }
  }
}
