import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { Quote } from 'src/app/interfaces/quote.interface';
import { DataService } from '../../../../services/data.service';

@Component({
  selector: 'app-quote',
  templateUrl: './quote.component.html',
  styleUrls: ['./quote.component.scss']
})
export class QuoteComponent {

  @Input() quote: Quote;
  @Input() listItem: boolean;

  constructor() {}
}
