import { TestBed } from '@angular/core/testing';

import { QuotesDataService } from './quotes-data.service';

describe('QuotesDataService', () => {
  let service: QuotesDataService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(QuotesDataService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
