import { Injectable } from '@angular/core';

import { Observable } from 'rxjs';
import { QuotesPagination } from 'src/app/interfaces/quotesPagination.interface';

import { DataService } from 'src/app/services/data.service';

@Injectable()
export class QuotesDataService {
  constructor(
    private readonly dataService: DataService
  ) {}

  public checkForFilters(
    tagFilter: string,
    pagination: number
  ): Observable<QuotesPagination> {
    if (tagFilter && pagination) {
      return this.checkForQuotesByTagNameAndPageCache(tagFilter, pagination);
    } else if (tagFilter) {
      return this.checkForQuotesByTagNameCache(tagFilter);
    } else {
      return this.checkForQuotesCache(pagination);
    }
  }

  public checkForQuotesCache(pagination: number) {
    return this.dataService.checkForQuotesCache(pagination ?? 1);
  }

  public handlePageChange(
    tagFilter: string,
    pageNumber: number
  ): Observable<QuotesPagination> {
    if (tagFilter) {
      return this.dataService.getQutesByTagNameAndPage(tagFilter, pageNumber);
    } else {
      return this.dataService.getNewQuotesPage(pageNumber);
    }
  }

  private checkForQuotesByTagNameAndPageCache(
    tagFilter: string,
    pagination: number
  ): Observable<QuotesPagination> {
    return this.dataService.checkForQuotesByTagNameAndPage(
      tagFilter,
      pagination
    );
  }

  private checkForQuotesByTagNameCache(
    pagination: string
  ): Observable<QuotesPagination> {
    return this.dataService.checkForQuotesByTagName(pagination);
  }
}
