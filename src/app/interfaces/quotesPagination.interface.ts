import { Quote } from "./quote.interface"

export interface QuotesPagination {
    count: number,
    lastItemIndex: number,
    page: number,
    results: Quote[]
    totalCount: number,
    totalPages: number
}