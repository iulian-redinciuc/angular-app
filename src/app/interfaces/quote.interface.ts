export interface Quote {
    author: string,
    authorSlug: string,
    content: string,
    dateAdded: string,
    dateModified: string,
    length: string,
    tags: string[]
    _id: string
}