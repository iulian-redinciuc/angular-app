export interface QuoteTag {
    dateAdded: string,
    dateModified: string,
    name: string,
    quoteCount: number
    __v: number
    _id: string
}