import { Component } from '@angular/core';
import { AuthenticationService } from './services/authentication.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'angular-app';

  constructor(private readonly authService: AuthenticationService) {}

  public isUserdLoggedIn() {
    return this.authService.isUsserLoggedIn();
  }

  public logOut() {
    //fake logout
    this.authService.logOut();
  }
}
