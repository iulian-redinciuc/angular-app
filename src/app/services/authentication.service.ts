import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, of } from 'rxjs';

import * as credentialsJSON from '../../assets/credentials.json';
import { UserCredentials } from '../interfaces/userCredentials.interface';

@Injectable()
export class AuthenticationService {
  constructor(private readonly router: Router) {}
  public login(credentials: UserCredentials): Observable<boolean> {
    const dbCredentials = (credentialsJSON as any).default;

    const loginSuccessfull = dbCredentials.users.some(
      (user: UserCredentials) =>
        user.email === credentials.email &&
        user.password === credentials.password
    );

    sessionStorage.setItem('isUserLoggedIn', loginSuccessfull);

    return of(loginSuccessfull);
  }

  public logOut(): void {
    this.router.navigate(['login']);
  }

  public isUsserLoggedIn(): boolean {
    const storeData = sessionStorage.getItem('isUserLoggedIn');
    return storeData !== null && storeData === 'true';
  }
}
