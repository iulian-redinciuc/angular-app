import { Injectable } from '@angular/core';

import { throwError } from 'rxjs';

@Injectable()
export class ErorrHandlingService {

  constructor() {}

  public handleError(error: any) {
    // handle different error cases
    alert('Something went wrong, please try again later');

    return throwError(() => error)
  }
}
