import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable, shareReplay} from 'rxjs';

import { QuotesPagination } from '../interfaces/quotesPagination.interface';
import { QuoteTag } from '../interfaces/quoteTag.interface';
import { Quote } from '../interfaces/quote.interface';


@Injectable()
export class DataService {
  private readonly endpoint = 'https://api.quotable.io';
   quotesCache: Observable<QuotesPagination>;
  private quotesTagsCache: Observable<QuoteTag[]>
  private quotesByTagNameCache: Observable<QuotesPagination>;
  private quotesByTagNameAndPageCache: Observable<QuotesPagination>;

  constructor(private readonly http: HttpClient) {}

  private get<DataType>(endpoint: string): Observable<any> {
    return this.http.get<DataType>(endpoint);
  }

  public checkForQuotesCache(pageNumber: number): Observable<QuotesPagination> {
    return !this.quotesCache ? this.quotesCache = this.getQuotesPage(pageNumber) : this.quotesCache;
  }

  public checkForQuotesTagsCache(): Observable<QuoteTag[]> {
    return !this.quotesTagsCache ? this.quotesTagsCache = this.getQuotesTags() : this.quotesTagsCache;
  }

  public checkForQuotesByTagName(tag: string): Observable<QuotesPagination> {
    return !this.quotesByTagNameCache ? this.quotesByTagNameCache = this.getQuotesByTagName(tag) : this.quotesByTagNameCache;
  }

  public checkForQuotesByTagNameAndPage(tag: string, page: number): Observable<QuotesPagination> {
    return !this.quotesByTagNameAndPageCache ? this.quotesByTagNameAndPageCache = this.getQutesByTagNameAndPage(tag, page) : this.quotesByTagNameAndPageCache;
  }

  public getQuoteById(quoteId: string): Observable<Quote> {
    return this.get<Quote>(`${this.endpoint}/quotes/${quoteId}`);
  }

  public getQuotesByTagName(tagName: string): Observable<QuotesPagination> {
    return this.get<QuotesPagination>(`${this.endpoint}/quotes?tags=${tagName}`).pipe(shareReplay(1));
  }

  public getNewQuotesPage(pageNumber: number): Observable<QuotesPagination> {
    return this.quotesCache = this.getQuotesPage(pageNumber)
  }
  
  public getQutesByTagNameAndPage(tag: string, page: number): Observable<any> {
    return this.get<QuotesPagination>(`${this.endpoint}/quotes?tags=${tag}&page=${page}`).pipe(shareReplay(1));
  }

  public getQuotesPage(pageNumber: number): Observable<QuotesPagination> {
    return this.get<QuotesPagination>(`${this.endpoint}/quotes?page=${pageNumber}`).pipe(shareReplay(1));
  }

  private getQuotesTags(): Observable<QuoteTag[]> {
    return this.get<QuoteTag[]>(`${this.endpoint}/tags`).pipe(shareReplay(1));;
  }
}
