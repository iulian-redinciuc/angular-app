import { TestBed } from '@angular/core/testing';

import { ErorrHandlingService } from './erorr-handling.service';

describe('ErorrHandlingService', () => {
  let service: ErorrHandlingService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ErorrHandlingService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
