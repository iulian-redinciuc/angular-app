import { AfterViewInit, Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { fromEvent, Subscription } from 'rxjs';

import { AuthenticationService } from '../../services/authentication.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, OnDestroy {
  @ViewChild('passwordInput') passwordInput: ElementRef;
  @ViewChild('showPassword') showPassword: ElementRef;

  loginForm: FormGroup;
  subscriptions: Subscription = new Subscription();

  constructor(private readonly authService: AuthenticationService, private readonly router: Router) { }

  ngOnInit(): void {
    this.loginForm = new FormGroup({
      email: new FormControl('', [Validators.required]),
      password: new FormControl('', [Validators.required])
    });
  }

  public onSubmit(): void {
    this.subscriptions.add(
      this.authService.login(this.loginForm.value).subscribe((isLoggedInt: boolean) => {
        if(isLoggedInt) {
          this.router.navigate(['dashboard']);
        } else {
          this.loginForm.setErrors({ 'noMatch': true })
          console.log(this.loginForm.hasError('noMatch'))
        }
      })
    );
  }

  ngOnDestroy(): void {
    this.subscriptions.unsubscribe()
  }

}
