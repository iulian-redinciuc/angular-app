import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { LoginComponent } from './components/login/login.component';
import { LogOutGuard } from './guards/log-out.guard';


const routes: Routes = [
  {
    path: 'login',
    component: LoginComponent,
    canActivate: [LogOutGuard]
  },
  {
    path: 'dashboard',
    loadChildren: () => import('./features/dashbord/dashbord.module').then(module => module.DashbordModule)
  },
  {
    path: '', 
    redirectTo: 'dashboard', 
    pathMatch: 'full'
  },
  {
    path: '**', 
    redirectTo: 'dashboard', 
    pathMatch: 'full'
  } 
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
